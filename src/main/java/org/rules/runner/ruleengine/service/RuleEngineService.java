package org.rules.runner.ruleengine.service;

import org.rules.runner.ruleengine.spi.RuleEngine;

import java.util.ServiceLoader;

public class RuleEngineService {

    private static RuleEngineService ruleEngineService;
    private final ServiceLoader<RuleEngine> loader;

    private RuleEngineService() {
        this.loader = ServiceLoader.load(RuleEngine.class);
    }

    public static synchronized RuleEngineService getInstance() {
        if (ruleEngineService == null)
            ruleEngineService = new RuleEngineService();
        return ruleEngineService;
    }

    public RuleEngine getRuleEngine() {
        return loader.findFirst().get();
    }

}
