package org.rules.runner.ruleengine.spi;

/**
 * A bare minimum {@code Service Provider Interface} for various rule engines.
 */
public interface RuleEngine {

    /**
     * The Rule Engine's description.
     *
     * @return the description
     */
    public String getDescription();

    /**
     * Initialize the rule engine with the set of supplied rules.
     * Assumes that the rules were supplied by {@code PersistedRuleProvider}.
     *
     * @param rules
     */
    public void setupRules(String rules);

    /**
     * Executes the rules on the supplied {@code inputData}.
     * This method has side-effects, the {@code result} object gets populated due to the execution of the rules.
     *
     * @param inputData
     */
    public <T, U> void execute(T inputData, U result);

}
