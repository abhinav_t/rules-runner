package org.rules.runner;

import org.rules.runner.persistedrules.service.PersistedRulesProviderService;
import org.rules.runner.persistedrules.spi.PersistedRulesProvider;
import org.rules.runner.ruleengine.service.RuleEngineService;
import org.rules.runner.ruleengine.spi.RuleEngine;

public class RulesRunner {

    private final RuleEngine ruleEngine;
    private final PersistedRulesProvider persistedRulesProvider;

    public RulesRunner() {
        this.ruleEngine = RuleEngineService.getInstance().getRuleEngine();
        this.persistedRulesProvider = PersistedRulesProviderService.getInstance().getPersistedRulesProvider();
        this.ruleEngine.setupRules(this.persistedRulesProvider.loadRules());
    }

    public <T, U> void execute(T inputData, U result) {
        this.ruleEngine.execute(inputData, result);
    }

    public RuleEngine getRuleEngine() {
        return ruleEngine;
    }

    public PersistedRulesProvider getPersistedRulesProvider() {
        return persistedRulesProvider;
    }

}
