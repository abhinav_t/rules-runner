package org.rules.runner.persistedrules.spi;

/**
 * A {@code Service Provider Interface} for fetching a set of business rules from various data-stores.
 *
 * @param <T> the type of the configuration object for the particular data-store
 */
public interface PersistedRulesProvider<T> {

    /**
     * Configures the provider so that it can fetch the rules from the particular data-source.
     *
     * @param configuration
     * @param <T>
     */
    public void configure(T configuration);

    /**
     * Fetch the rules!
     *
     * @return
     */
    public String loadRules();

    /**
     * Update the rules with the ones supplied here.
     *
     * @param rules
     */
    public void updateRules(String rules);

}
