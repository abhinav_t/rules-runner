package org.rules.runner.persistedrules.service;

import org.rules.runner.persistedrules.spi.PersistedRulesProvider;

import java.util.ServiceLoader;

public class PersistedRulesProviderService {

    private static PersistedRulesProviderService persistedRulesProviderService;
    private final ServiceLoader<PersistedRulesProvider> loader;

    private PersistedRulesProviderService() {
        this.loader = ServiceLoader.load(PersistedRulesProvider.class);
    }

    public static synchronized PersistedRulesProviderService getInstance() {
        if (persistedRulesProviderService == null)
            persistedRulesProviderService = new PersistedRulesProviderService();
        return persistedRulesProviderService;
    }

    public PersistedRulesProvider getPersistedRulesProvider() {
        return loader.findFirst().get();
    }

}
