# Rules Runner

Rules Runner is a bare minimum layer that facilitates plugging in different datastores for rules and different kinds of rule engines. 
It uses the ServiceLoader class that comes with the JDK to define service provider interfaces (SPI) for rule engine providers and rule store providers. 
This is on the same lines as the JDBC API and various database drivers implementing it.

## Getting Started

### Prerequisites
* JDK 9+
* Maven

### Building and Running

Clone the repository in your local machine, `master` branch is the last stable branch.

```
    git clone https://abhinav_t@bitbucket.org/abhinav_t/rules-runner.git
    cd rules-runner
    mvn clean install
```

Once this is available in Artifactory, you can include it in your project simply like this,

```
<dependency>
    <groupId>org.rules.runner</groupId>
    <artifactId>rules-runner</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

## Environments
Not Applicable. This is meant to be used as a library by different services which need a simple and flexible layer around rule engines.

## Deployment
Not Applicable. This is meant to be used as a library by different services which need a simple and flexible layer around rule engines.

## Dashboards
Not Applicable. This is meant to be used as a library by different services which need a simple and flexible layer around rule engines.

## Contributing
As of now, this is just a POC. However, if you have any refactoring improvements or new feature code to contribute, please reach out to `abhinav.tripathi@freecharge.com`.

## Contact Us
For any questions please reach out to `abhinav.tripathi@freecharge.com`.
